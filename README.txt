This sandbox module allows populating an AddressField from a Geofield lat/lon 
pair, using Google Maps API.

For now, only Google API is available for reverse geocoding. Other APIs (Bing, 
Yandex) could be added in the future.

## Installation
Remember that this module has dependence on both Address Field and Geofield 
modules.

## Usage
- Create at least a Geofield and an Address Field on your content type.
- Change the Address Field widget to "Populate from Geofield".
- In the Address Field settings, chose which Geofield to populate from.

## Author

Renaud CUNY (d.o: renaudcuny)
http://drupal.org/user/344486

## Special thanks to

- xandeadx for the useful code I found in this sandbox:
https://drupal.org/sandbox/xandeadx/1999792
